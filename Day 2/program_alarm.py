def intcode(seq, offset=0):
    opcode = seq[0 + offset]
    if opcode == 99:
        return seq
    vals = [seq[seq[1 + offset]], seq[seq[2 + offset]]]
    index = seq[3 + offset]

    if opcode == 1:
        seq[index] = sum(vals)
        return intcode(seq, offset + 4)
    elif opcode == 2:
        seq[index] = vals[0] * vals[1]
        return intcode(seq, offset + 4)

    else:
        return seq


with open("C:\Coding\Advent Of Code 2019\Day 2\input.txt") as input:
    main_sequence = list(map(int, input.read().split(",")))

for i in range(100):
    for j in range(100):

        sequence = main_sequence[:]
        sequence[1] = i
        sequence[2] = j
        print(intcode(sequence)[0], i, j)
        if intcode(sequence)[0] == 19690720:
            print("Answer: ", 100 * i + j)
            quit()

