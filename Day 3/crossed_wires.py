# import numpy as np

with open("Day 3/input.txt") as input:
    wire_a, wire_b = input.read().split("\n")
    wire_a = wire_a.split(",")
    wire_b = wire_b.split(",")

STEPS = {"U": (0, 1), "D": (0, -1), "L": (-1, 0), "R": (1, 0)}


def route(wire):
    path = []
    x, y = 0, 0

    for seg in wire:
        direction = STEPS[seg[0]]
        for _ in range(int(seg[1:])):
            x += direction[0]
            y += direction[1]

            path.append((x, y))
    return path


path_a = route(wire_a)
path_b = route(wire_b)

crosses = set(path_a).intersection(set(path_b))

manhattan = None

for cross in crosses:
    dist = abs(cross[0]) + abs(cross[1])
    if manhattan is None or dist < manhattan:
        manhattan = dist

print(manhattan)
