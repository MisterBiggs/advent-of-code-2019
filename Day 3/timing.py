with open("Day 3/input.txt") as input:
    wire_a, wire_b = input.read().split("\n")
    wire_a = wire_a.split(",")
    wire_b = wire_b.split(",")


STEPS = {"U": (0, 1), "D": (0, -1), "L": (-1, 0), "R": (1, 0)}


def route(wire):
    path = {}
    x, y = 0, 0
    count = 0
    for seg in wire:
        direction = STEPS[seg[0]]
        for _ in range(int(seg[1:])):
            x += direction[0]
            y += direction[1]
            count += 1
            path[(x, y)] = count
    return path


path_a = route(wire_a)
path_b = route(wire_b)

crosses = set(path_a.keys()).intersection(set(path_b.keys()))

minimum = None

for cross in crosses:
    steps = path_a[cross] + path_b[cross]

    if minimum is None or minimum > steps:
        minimum = steps

print(minimum)
