def intcode(seq, offset=0):
    parameter = str(seq[offset])
    parameter = parameter.rjust(5, "0")
    opcode = int(parameter[-2:])

    parameters = [int(x) for x in parameter[:-2][::-1]]

    params = len(parameters)

    for parameter in parameters:
        if parameter == 0:
            index = seq[3 + offset]

        if opcode == 99:
            return seq
        vals = [seq[seq[1 + offset]], seq[seq[2 + offset]]]

        if opcode == 1:
            seq[index] = sum(vals)
            return intcode(seq, offset + 4)
        elif opcode == 2:
            seq[index] = vals[0] * vals[1]
            return intcode(seq, offset + 4)
        elif opcode == 3:
            seq[index] = inp
            return intcode(seq, offset + 4)
        elif opcode == 4:
            seq[index] = out
            return intcode(seq, offset + 4)

        else:
            return seq


# with open("C:\Coding\Advent Of Code 2019\Day 5\input.txt") as input:
#     main_sequence = list(map(int, input.read().split(",")))

print(intcode([1002, 4, 3, 4, 33]))

