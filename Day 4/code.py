start = 272091
end = 815432
count = 0

for i in range(start, end):
    code = [int(x) for x in list(str(i))]

    if code == sorted(code):
        for c in code:
            if code.count(c) == 2:
                count += 1
                break
print(count)
